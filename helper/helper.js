const userModel = require('../model/userModel');
const jwt = require('jsonwebtoken');


exports.verifyToken = (req,res,next)=>{
    if(req.headers.token){
    jwt.verify(req.headers.token, process.env.JWTSECREATEKEY,(error,result)=>{
        if(error){return res.status(500).send({status:0,messege:"Please provided correct token"})}
        else{
            userModel.findOne({_id:result._id},(error,result)=>{
            if(error){return res.status(500).send({status:0,messege:"internal server error"})}   
            else if(result.userStatus == "BLOCK"){return res.status(409).send({status:0,messege:"you are blocked by Admin"})}
            else if(result.userStatus == "DELETE"){return res.status(409).send({status:0,messege:"you are deleted by admin"})} 
            else{
                req.user = result;
                next();
            } })
        }
    })
    }else{
        return res.status(409).send({status:0,messege:"temporery error occured"})
    }
}
// >>>>>>>>>>>>>>>>>>>>>>>>This is end of verifyToken Process<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
    








































// This is end of module exports statement