const mongoose = require("mongoose");

// connection start
mongoose.connect(
  process.env.MONGOURI,
  {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  },
  (error) => {
    if (error) {
      console.log(error);
    } else {
      console.log("db connected successfully");
    }
  }
);
// connection closed
