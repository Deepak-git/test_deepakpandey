const express = require("express");
const router = express.Router();
const userController = require("../controller/userController");
const { check } = require("express-validator/check");
const auth = require("../helper/helper");

router.post(
  "/signup",
  [
    check("userEmail").isEmail(),
    check("userPassword").isLength({ min: 5, max: 10 }),
  ],
  userController.signup
);

// This is end of signup route
router.get("/emailVerify/:userEmail", userController.emailVerify);
// This is end of emailVerify Route
router.post(
  "/forgetPassword",
  [
    check("userEmail").isEmail(),
    check("newPassword").isLength({ min: 5, max: 10 }),
  ],
  userController.forgetPassword
);
// This is end of fogetPassword route
router.post(
  "/login",
  [
    check("userEmail").isEmail(),
    check("userPassword").isLength({ min: 5, max: 10 }),
  ],
  userController.login
);
// This is end of login route

//  proteacted route only for login user
router.post(
  "/changePassword",
  [check("newPassword").isLength({ min: 5, max: 10 })],
  auth.verifyToken,
  userController.changePassword
);
// This is end of change password route
router.patch("/updateProfile", auth.verifyToken, userController.updateProfile);
// This is end of update password route
router.get("/getProfile", auth.verifyToken, userController.getProfile);
// This is end of get Profile route

module.exports = router;
