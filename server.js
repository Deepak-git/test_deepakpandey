require("dotenv").config();
require("./dbConfig/dbCon");
const express = require("express");
const bodyParser = require("body-parser");
const port = process.env.PORT || 5000;
const userRouter = require("./router/userRouter");

// initilize app
const app = express();

// Initilize midileware
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

// initilize route

app.use("/user", userRouter);

app.listen(port, () => {
  console.log(`server is live at ${port}`);
});
