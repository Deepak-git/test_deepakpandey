const userModel = require("../model/userModel");
const { validationResult } = require("express-validator");
const bcrypt = require("bcryptjs");
const nodemailer = require("nodemailer");
const jwt = require("jsonwebtoken");

module.exports = {
  signup: (req, res) => {
    try {
      const errors = validationResult(req);
      if (!req.body.userEmail) {
        return res
          .status(404)
          .send({ status: 0, messege: "userEmail required" });
      } else if (!req.body.userPassword) {
        return res
          .status(404)
          .send({ status: 0, messege: "userPassword required" });
      } else if (!errors.isEmpty()) {
        return res
          .status(400)
          .send({
            status: 0,
            messege: "please provided valid input",
            errors: errors.array(),
          });
      } else {
        userModel.findOne(
          {
            $and: [
              { userEmail: req.body.userEmail },
              { userStatus: { $in: ["ACTIVE", "BLOCK"] } },
            ],
          },
          (findErr, findRes) => {
            if (findErr) {
              return res
                .status(500)
                .send({ status: 0, messege: "internal server error" });
            } else if (findRes) {
              return res
                .status(400)
                .send({ status: 0, messege: "user already exist" });
            } else {
              var hash = bcrypt.hashSync(req.body.userPassword, 10);
              var vhtml = `http://localhost:3000/user/emailVerify/${req.body.userEmail}`;
              var transporter = nodemailer.createTransport({
                service: process.env.SERVICE_NAME,
                auth: {
                  user: process.env.AUTH_EMAIL,
                  pass: process.env.AUTH_PASSWORD,
                },
              });
              var mailOptions = {
                from: process.env.AUTH_EMAIL,
                to: req.body.userEmail,
                subject: "Regester user",
                text: `Dear ${req.body.userName},
                                 Thanks for join with us.click this link to verify it you : ${vhtml}`,
              };
              transporter.sendMail(mailOptions, (error, info) => {
                if (error) {
                  return res
                    .status(500)
                    .send({
                      status: 0,
                      messege: "temporery error occured",
                      error,
                    });
                } else {
                  var user = {
                    userName: req.body.userName,
                    userEmail: req.body.userEmail,
                    userPassword: hash,
                    userMobile: req.body.userMobile,
                    profileImg: req.body.profileImg,
                  };

                  new userModel(user).save((saveErr, saveRes) => {
                    if (saveErr) {
                      return res
                        .status(500)
                        .send({ status: 0, messege: "internal server error" });
                    } else if (!saveRes) {
                      return res
                        .status(400)
                        .send({ status: 0, messege: "error in signup" });
                    } else {
                      return res
                        .status(200)
                        .send({
                          status: 1,
                          messege: "signup successfull",
                          saveRes,
                        });
                    }
                  });
                }
              });
            }
          }
        );
      }
    } catch (error) {
      return res
        .status(501)
        .send({ status: 0, messege: "temporery error occured" });
    }
  },

  // This is end of signup APi
  emailVerify: (req, res) => {
    try {
      userModel.findOne(
        {
          $and: [
            { userEmail: req.params.userEmail },
            { userStatus: { $in: ["ACTIVE"] } },
          ],
        },
        (findErr, findRes) => {
          if (findErr) {
            return res
              .status(500)
              .send({ status: 0, messege: "internal server error" });
          } else if (!findRes) {
            return res
              .status(404)
              .send({ status: 0, messege: "user not found" });
          } else {
            userModel.findByIdAndUpdate(
              { _id: findRes._id },
              { $set: { emailVerify: true } },
              { new: true },
              (upErr, upRes) => {
                if (upErr) {
                  return res
                    .status(500)
                    .send({ status: 0, messege: "internal server error" });
                } else if (!upRes) {
                  return res
                    .status(409)
                    .send({ status: 0, messege: "error in email verify" });
                } else {
                  return res
                    .status(200)
                    .send({ status: 0, messege: "email verify successfully" });
                }
              }
            );
          }
        }
      );
    } catch (error) {
      return res
        .status(501)
        .send({ status: 0, messege: "temporery error occured" });
    }
  },
  //    This is end of emailVerify

  login: (req, res) => {
    try {
      const errors = validationResult(req);
      if (!req.body.userEmail) {
        return res
          .status(404)
          .send({ status: 0, messege: "userEmail required" });
      } else if (!req.body.userPassword) {
        return res
          .status(404)
          .send({ status: 0, messege: "userPassword required" });
      } else if (!errors.isEmpty()) {
        return res
          .status(400)
          .send({
            status: 0,
            messege: "please provided valid input",
            errors: errors.array(),
          });
      } else {
        userModel.findOne(
          {
            $and: [
              { userEmail: req.body.userEmail },
              { userStatus: { $in: ["ACTIVE"] } },
            ],
          },
          (findErr, findRes) => {
            if (findErr) {
              return res
                .status(500)
                .send({ status: 0, messege: "internal server errror" });
            } else if (!findRes) {
              return res
                .status(409)
                .send({ status: 0, messege: "please login first" });
            } else if (findRes.emailVerify === false) {
              return res
                .status(409)
                .send({ status: 0, messege: "verify email first" });
            } else {
              var pass = bcrypt.compareSync(
                req.body.userPassword,
                findRes.userPassword
              );
              if (pass === true) {
                jwt.sign(
                  { _id: findRes._id },
                  process.env.JWTSECREATEKEY,
                  { expiresIn: "1h" },
                  (error, token) => {
                    if (error) {
                      return res
                        .status(500)
                        .send({
                          status: 0,
                          messege: "internal server error....",
                        });
                    } else {
                      res
                        .status(200)
                        .send({
                          status: 1,
                          messege: "login successfull",
                          token,
                        });
                    }
                  }
                );
              } else {
                return res
                  .status(409)
                  .send({
                    status: 0,
                    messege: "userEmail or userPassword is wrong",
                  });
              }
            }
          }
        );
      }
    } catch (error) {
      return res
        .status(501)
        .send({ status: 0, messege: "temporery error occured" });
    }
  },
  // This is end of login Api
  forgetPassword: (req, res) => {
    try {
      const errors = validationResult(req);
      if (!req.body.userEmail) {
        return res
          .status(404)
          .send({ status: 0, messege: "userEmail required" });
      }
      if (!req.body.newPassword) {
        return res
          .status(404)
          .send({ status: 0, messege: "newPassword required" });
      } else if (!errors.isEmpty()) {
        return res
          .status(400)
          .send({
            status: 0,
            messege: "please provided valid input",
            errors: errors.array(),
          });
      } else {
        userModel.findOne(
          { userEmail: req.body.userEmail, userStatus: { $in: ["ACTIVE"] } },
          (findErr, findRes) => {
            if (findErr) {
              return res
                .status(500)
                .send({ status: 0, messege: "internal server error" });
            } else if (!findRes) {
              return res
                .status(409)
                .send({ status: 0, messege: "user not found" });
            } else {
              var newHash = bcrypt.hashSync(req.body.newPassword, 10);
              userModel.findByIdAndUpdate(
                { _id: findRes._id },
                { $set: { userPassword: newHash, emailVerify: false } },
                { new: true },
                (upErr, upRes) => {
                  if (upErr) {
                    return res
                      .status(500)
                      .send({ status: 0, messege: "internal server error" });
                  } else if (!upRes) {
                    return res
                      .status(409)
                      .send({ status: 0, messege: "some error occured" });
                  } else {
                    return res
                      .status(200)
                      .send({
                        status: 1,
                        messege: "password updated successfully",
                      });
                  }
                }
              );
            }
          }
        );
      }
    } catch (error) {
      return res
        .status(509)
        .send({ status: 0, messege: "temporery error occured" });
    }
  },
  // This is end of forget password
  changePassword: (req, res) => {
    try {
      const errors = validationResult(req);
      if (!req.body.newPassword) {
        return res
          .status(404)
          .send({ status: 0, messege: "newPassword required" });
      } else if (!req.user) {
        return res.status(404).send({ status: 0, messege: "user login must" });
      } else if (!errors.isEmpty()) {
        return res
          .status(400)
          .send({
            status: 0,
            messege: "please provided valid input",
            errors: errors.array(),
          });
      } else {
        userModel.findOne({ _id: req.user._id }, (findErr, findRes) => {
          if (findErr) {
            return res
              .status(500)
              .send({ status: 0, messege: "internal server error" });
          } else {
            var newHash = bcrypt.hashSync(req.body.newPassword, 10);
            userModel.findByIdAndUpdate(
              { _id: findRes._id },
              { $set: { userPassword: newHash, emailVerify: false } },
              { new: true },
              (upErr, upRes) => {
                if (upErr) {
                  return res
                    .status(500)
                    .send({ status: 0, messege: "internal server error" });
                } else if (!upRes) {
                  return res
                    .status(400)
                    .send({ status: 0, messege: "error in change password" });
                } else {
                  return res
                    .status(200)
                    .send({
                      status: 1,
                      messege: "password updated successfully",
                    });
                }
              }
            );
          }
        });
      }
    } catch (error) {
      return res
        .status(501)
        .send({ status: 0, messege: "temporery error occured" });
    }
  },
  //   This is end of change password Api

  updateProfile: (req, res) => {
    try {
      if (!req.user) {
        return res
          .status(409)
          .send({ status: 0, messege: "user login required" });
      } else {
        userModel.findOne({ _id: req.user._id }, (findErr, findRes) => {
          if (findErr) {
            return res
              .status(500)
              .send({ status: 0, messege: "internal server error" });
          } else if (!findRes) {
            return res
              .status(404)
              .send({ status: 0, messege: "user not found" });
          } else {
            var set = {};
            if (req.body.userName) {
              set["userName"] = req.body.userName;
            }
            if (req.body.userMobile) {
              set["userMobile"] = req.body.userMobile;
            }
            if (req.body.profileImg) {
              set["profileImg"] = req.body.profileImg;
            }
            userModel.findByIdAndUpdate(
              { _id: findRes._id },
              { $set: set },
              { new: true },
              (upErr, upRes) => {
                if (upErr) {
                  return res
                    .status(500)
                    .send({ status: 0, messege: "internal server error" });
                } else if (!upRes) {
                  return res
                    .status(400)
                    .send({ status: 0, messege: "error in update profile" });
                } else {
                  return res
                    .status(200)
                    .send({
                      status: 1,
                      messege: "profile update successfully",
                    });
                }
              }
            );
          }
        });
      }
    } catch (error) {
      return res
        .status(501)
        .send({ status: 0, messege: "temporery error occured" });
    }
  },
  // This is end of update profile

  getProfile: (req, res) => {
    try {
      if (!req.user) {
        return res
          .status(409)
          .send({ status: 0, messege: "user must be login" });
      } else {
        userModel.findOne({ _id: req.user }, (findErr, findRes) => {
          if (findErr) {
            return res
              .status(500)
              .send({ status: 0, messege: "internal server error" });
          } else if (!findRes) {
            return res
              .status(409)
              .send({ status: 0, messege: "profile not found" });
          } else {
            return res
              .status(200)
              .send({
                status: 1,
                messege: "profile found successfully",
                findRes,
              });
          }
        });
      }
    } catch (error) {
      return res
        .status(501)
        .send({ status: 0, messege: "temporery error occured" });
    }
  },
};

// This is end of module exports statement
