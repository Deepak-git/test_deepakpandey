const mongoose = require("mongoose");
const userSchema = new mongoose.Schema(
  {
    userName: {
      type: String,
      required: true,
    },
    userEmail: {
      type: String,
      required: true,
    },
    userPassword: {
      type: String,
      required: true,
    },
    userType: {
      type: String,
      enum: ["ADMIN", "USER"],
      default: "USER",
    },
    userStatus: {
      type: String,
      enum: ["ACTIVE", "BLOCK", "DELETE"],
      default: "ACTIVE",
    },
    userMobile: {
      type: String,
    },
    profileImg: {
      type: String,
    },
    emailVerify: {
      type: Boolean,
      default: "false",
    },
  },
  { timestamps: true }
);

module.exports = mongoose.model("userModel", userSchema);
